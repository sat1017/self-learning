# Self Learning
## Programming
I completed Cal Berkley's Structure and Interpretation of Computer Programs (CS61A) course. I learned about controlling program complexity, such as functional programming, recursion, data abstraction, and object-oriented programming.

Key takeaways:
- Lectures Completed: 33
- Homeworks Completed: 8
- Notes Takes: 78 pages
- Hours Spent: 200

## Computer Architecture
I read The C Programming Language book and completed most of the exercises in the book. I learned about types, operators, expressions, control flow, pointers, pointer arithmetic, arrays, structures, and I/O.

Key takeaways:
- Pages Read: 188
- Exercises Completed: 61
- Hours Spent: 200

I read Computer Systems: A Programmer's Perspective. I learned about the physical computer system, representing and manipulating information, machine-level representation of programs, memory hierarchy, exceptional control flow, virtual memory, system-level I/O, networking programming, and concurrent programming.

Key takeaways:
- Pages Read: 998
- Notes Takes: 66 pages
- Hours Spent: 200

## Algorithms and Data Structures
I completed Cal Berkley's Data Structures (CS61B) course. I learned about Java, data structures like linked lists, BSTs, B-trees, red black trees, minimim spanning trees, sets, hash tables, heaps, queues, and tries, tree and graph traversals like Kruskal's, Prim's, and Dijkstra's shortest paths, and sorting algorithms like selection sort, quick sort, heap sort, merge sort, insertion sort, and radix sort. I started with the Algorithm Design Manual but found it too theoretically heavy for a beginner.

Key takeaways:
- Lectures Completed: 31
- Discussions Completed: 12
- Notes Takes: 156 pages
- Hours Spent: 300

## Distributed Systems
I read Designing Data-Intensive Applications. I learned about data models and query languages, storage and retrieval, encoding and evolution, replication, partitioning, transactions, consistency and consensus, and batch and stream processing.

Key takeaways:
- Pages Read: 31
- Notes Takes: 20
- Hours Spent: 50
