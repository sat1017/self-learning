package disc05;
import java.util.Iterator;
import java.util.NoSuchElementException;
/*
First, let’s define an iterator. Create a class OHIterator that implements an iterator
over OHRequest objects that only returns requests with good descriptions. Our
OHIterator’s constructor will take in an OHRequest object that represents the first
OHRequest object on the queue. We’ve provided a function, isGood, that accepts a
description and says if the description is good or not. If we run out of office hour
requests, we should throw a NoSuchElementException when our iterator tries to get
another request.
*/
/* Steps to implement Iterators
    1. have public class implements Iterator<T>
    2. create @Override hasNext()
    3. create @Override next()
   Steps to implement Iterable
    1. have public class implement Iterable<T>
    2. create @Override Iterator<T> iterator() that returns constructor from step 3
    3. create class constructor that implements Iterator<T>
    4. create @Override hasNext()
    5. create @Override next()
 */
public class OHIterator implements Iterator<OHRequest> {
    OHRequest curr;

    public OHIterator(OHRequest queue) {
        curr = queue;
    }

    public boolean isGood(String description) {
        return description != null && description.length() > 5;
    }

    @Override
    public boolean hasNext() {
        while (curr != null && !isGood(curr.description)) {
            curr = curr.next;
        }
        if (curr == null) {
            return false;
        }
        return true;
    }

    @Override
    public OHRequest next() {
        if (!hasNext()) {
            throw new NoSuchElementException();
        }
        OHRequest currRequest = curr;
        curr = curr.next;
        return currRequest;
    }
}
