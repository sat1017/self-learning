package disc07;
/* 1 Binary Search Trees
(a) For a BST that contains N items, what is the asymptotic worst case of each
    for each of these functions?
        i. add(int x) O(N)
        ii. getMin() O(N)

(b) Now assume that the BST from part (a) is certain to be bushy. What is our
    new asymptotic worst case?
        i. add(int x) O(logN)
        ii. getMin() O(logN)

(c) Let’s implement the find function in our BSTMap. It should take in an integer
    and return the value associated with that key or null if the key is not in our
    BSTMap.
 */

public class BSTMap {
    private class Node {
        int key;
        int value;
        Node left;
        Node right;
        Node (int key, int value) { ... }
    }
    Node head; // Contains the node at the head of the tree

    public Integer find(int key) {
        return findHelper(key, head);
    }
    private Integer findHelper(int key, Node n) {
        if (n.key == key) {
            return n.value;
        } else if (n == null) {
            return null;
        } else if (n.key < key) {
            return findHelper(key, n.left);
        } else {
            return findHelper(key, n.right);
        }
    }
}
