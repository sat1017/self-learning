package disc03;

public class SLList { //can be called from another file
    private class IntNode { //cannot be called from another file
        public int item;
        public IntNode next;
        public IntNode(int item, IntNode next) {
            this.item = item;
            this.next = next;
        }
    }
    private IntNode first;

    public SLList(int x) {
        first = new IntNode(x, null);
    }

    public void addFirst(int x) {
        first = new IntNode(x, first);
    }
    /**
     * Implement SLList.insert which takes in an integer x and an integer position.
     * It inserts x at the given position. If position is after the end of the list, insert
     * the new node at the end.
     *
     * For example, if the SLList is 5 → 6 → 2, insert(10, 1) results in 5 → 10 → 6 → 2
     * and if the SLList is 5 → 6 → 2, insert(10, 7) results in 5 → 6 → 2 → 10.
     * Additionally, for this problem assume that position is a non-negative integer
     */
    public void insert(int x, int position) {
        if (first == null || position == 0) {
            addFirst(x);
            return;
        }
        IntNode currentNode = first;
        while (currentNode.next != null && position > 1) {
            System.out.println(position);
            position--;
            currentNode = currentNode.next;
        }
        IntNode newNode = new IntNode(x, currentNode.next); // create new node pointing at it's prev's next
        // newNode will be thrown out after func call
        currentNode.next = newNode; // create prev's next to point at the new node
    }
    /**
     * Add another method to SLList that recursively removes all nodes that contain
     * a certain item. This method takes in an integer x and destructively changes
     * the list.
     * For example, if the SLList is 3 → 5 → 4 → 5 → 6 → 5, removeItem(5) results
     * in 3 → 4 → 6.
     */
    public void removeItem(int x) {
        first = removeItemHelper(x, first);
    }
    private IntNode removeItemHelper(int x, IntNode current) {
        if (current == null) {
            return null;
        } else if (current.item == x) {
            return removeItemHelper(x, current.next);
        } else {
            current.next = removeItemHelper(x, current.next);
            return current;
        }
    }
    public static void main(String[] args) {
        SLList a1 = new SLList(5);
        a1.addFirst(6);
        a1.addFirst(5);
        a1.insert(10, 1); // {5, 10, 6, 2}
        a1.removeItem(5);
    }
}

