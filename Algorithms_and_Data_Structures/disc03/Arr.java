package disc03;

public class Arr {

    public static int[] insert(int [] arr, int item, int position){
        /**
         * create new list with size + 1
         * for [:position]
         *  copy old to new
         * new[pos] = item
         * for [position+1:]
         *  copy old to new
         */

        int [] result = new int[arr.length + 1];
        int len = Math.min(arr.length, position);
        for (int i = 0; i < len; i++) {
            result[i] = arr[i];
        }
        result[position] = item;

        for (int i = position; i < arr.length; i++) {
            result[i+1] = arr[i];
        }
        return result;
    }
    public static int[] replicate(int[] arr) {
        /** Write a non-destructive method replicate(int[] arr)
         * that replaces the number at index i with arr[i] copies of
         * itself. For example, replicate([3, 2, 1]) would return
         * [3, 3, 3, 2, 2, 1]. For this question assume that all elements
         of the array are positive.
         */
        int total = 0;
        for (int i = 0; i < arr.length; i++) {
            total += arr[i];
        }
        int [] new_arr = new int[total];
        int i = 0;
        for (int item : arr) {
            for (int counter = 0; counter < item; count++) {
                new_arr[i] = item;
                i++;
            }
        }
        return new_arr;
    }

    public static void main(String[] args) {
        int [] arr = new int[]{5, 9, 14, 15};
        int [] result = insert(arr, 6, 2);
        System.out.println(result);
        System.out.println(replicate(new int[]{3, 2, 1}));
    }
}
