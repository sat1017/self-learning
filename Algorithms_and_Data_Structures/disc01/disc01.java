public class disc01 {
// Question 2
	public static int mystery(int[] inputArray, int k) {
		int x = inputArray[k];
		int answer = k;
		int index = k + 1;
		while (index < inputArray.length) {
			if (inputArray[index] < x) {
				x = inputArray[index];
				answer = index;
			}
			index = index + 1;
		}
		return answer;
	}

	/* a) the function returns 4 
	   b) the function finds the smallest number in the array at or after index k
	*/

	// Question 3
	// implement fib sequence for int n

	public static int fib(int n) {
		if ( n <= 1) {
			return n;
		} else {
			return fib(n-1) + fib(n-2);
		}
	}
	
	public static void main(String[] args) {
		int [] inputArray = {3, 0, 4, 6, 3};
		System.out.println(mystery(inputArray, 2));
		int n = 7;
		System.out.println(fib(n));
	}
}

