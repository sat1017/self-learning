
1 Disjoint Sets, a.k.a. Union Find

In lecture, we discussed the Disjoint Sets ADT. Some authors call this the Union
Find ADT. Today, we will use union find terminology so that you have seen both.
(a) What are the last two improvements (out of four) that we made to our naive
implementation of the Union Find ADT during lecture 14 (Monday’s lecture)?

1. Improvement 1: Weighted Quick Union
2. Improvement 2: Weighted Quick Union w/ Path Compression

(b) Assume we have nine items, represented by integers 0 through 8. All items are
initially unconnected to each other. Draw the union find tree, draw its array
representation after the series of connect() and find() operations, and write
down the result of find() operations using WeightedQuickUnion. Break
ties by choosing the smaller integer to be the root.

Note: find(x) returns the root of the tree for item x.

connect(2,
connect(1,
connect(5,
connect(8,
connect(7,
find(3);
connect(0,
connect(6,
connect(6,
find(8);
find(6);

int:  -2 2 -9 2 0 2 0 5 0
index: 0 1  2 3 4 5 6 7 8
find(3) --> 2
find(8) --> 2
find(6) --> 2

                       2
                    / / \ \
                   3 1  5  0
                       /   / \
                      7   6   4
                               \
                                8


(c) Repeat the above part, using WeightedQuickUnion with Path Compres-
sion.

int:   2 2 -9 2 2 2 2 5 2
index: 0 1  2 3 4 5 6 7 8
find(3) --> 2
find(8) --> 2
find(6) --> 2

                          2
                    / / / | \ \ \
                   0 1  3 4  5 6 8
                            /
                            7