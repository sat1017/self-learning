public class Cat {
	public String name;
	public static String noise;

	public Cat(String name, String noise) {
		this.name = name;
		this.noise = noise;		
	}

	public void play() {
		System.out.println(noise + "I'm " + name + " the cat!");
	}
	public void nickname(String newName) {
		name = newName;
	}

	public static void anger() {
		noise = noise.toUpperCase();
	}

	public static void calm() {
		noise = noise.toLowerCase();
	}
	
	// Question 2: What will each line in main print?
	public static void main(String[] args) {
		Cat a = new Cat("Cream", "Meow!"); // creates a Cat instance
		Cat b = new Cat("Tubbs", "Nyan!"); // creates a Cat instance, changes the class attribute noise
		a.play();						   // Nyan! I'm Cream the cat!
		b.play();						   // Nyan! I'm Tubbs the cat!
		Cat.anger();					   // updates the class attribute to NYAN!
		a.calm();						   // updates the class attribute to nyan!
		a.play();						   // nyan! I'm Cream the cat!
		b.play();						   // nyan! I'm Tubbs the cat!
		a.nickname("Kitty");			   // updates the instance a name attribute to Kitty 
		a.play();						   // nyan! I'm Kitty the cat!
		b.play();						   // nyan! I'm Tubbs the cat!

		// Cat.nickname("Kitkat")
		/** If we were to add this line to our main function, it would error. In the class,
			nickname is an instance function. What would it mean to rename Cat as
			opposed to a specific cat? It doesn’t really make sense. So when we try to run
			this function on our class, it errors.
		    The functions anger and calm are declared static themselves. Static methods can 
			be called using the name of the class, as in line 7, whereas non-static methods 
			cannot. The golden rule for static methods to know is that static methods can
			only modify static variables.*/
	}
	
}
