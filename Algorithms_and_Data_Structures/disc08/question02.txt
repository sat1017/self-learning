Question 2: Hashing
(a) Here are three potential implementations of the Integer’s hashCode() function.
Categorize each as either a valid or an invalid hash function. If it is invalid,
explain why. If it is valid, point out a flaw or disadvantage.

public int hashCode() {
    return -1;
}

Valid. As required, this hash function returns the same hashCode for Integers that 
are equals() to each other. However, this is a bad implementation since there will 
be collision 100% of the time.

public int hashCode() {
    return intValue() * intValue();
}

Valid. Similar to (a), this hash function returns the same hashCode for integers
that are equals(). However, integers that share the same absolute values will
collide (for example, x = 5 and x = −5 will have the same hash code). A
better hash function would be to just return the intValue() itself.

public int hashCode() {
    return super.hashCode();
}

Invalid. This is not a valid hash function because integers that are equals()
to each other will not have the same hash code. Instead, this hash function
returns some integer corresponding to the integer object’s location in memory.

(b) For each of the following questions, answer Always, Sometimes, or Never.
    1. When you modify a key that has been inserted into a HashMap will you be
    able to retrieve that entry again? Explain.

    Sometimes. If the hashCode for the key happens to change as a result of
    the modification, then we won’t be able to reliably retrieve the key.

    2. When you modify a value that has been inserted into a HashMap will you
    be able to retrieve that entry again? Explain.

    Always. The bucket index for an entry in a HashMap is decided by the key,
    not the value. Mutating the value does not affect the lookup procedure.