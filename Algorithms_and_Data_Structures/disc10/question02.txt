Question 2: Dijkstra’s Algorithm

For the graph below, let g(u, v) be the weight of the edge between any nodes u
and v. Let h(u, v) be the value returned by the heuristic for any nodes u and v.

(a) Run Dijkstra’s algorithm to find the shortest paths from A to every other g(E, G) = 3
vertex. You may find it helpful to keep track of the priority queue and make
a table of current distances.

Mental Math:
A -> B 1
A -> C 4
A -> F 6
A -> G 7
A -> D 2
A -> E 5

Algo:
Explanation: We will maintain a priority queue and a table of distances
found so far, as suggested in the problem and pseudocode. We will use {} to
represent the PQ, and (()) to represent the table of distances.

{A:0, B:inf, C:inf, D:inf, E:inf, F:inf, G:inf}. (()).

Pop A.

{B:inf, C:inf, D:inf, E:inf, F:inf, G:inf}. ((A: 0)).

changePriority(B, 1). changePriority(D, 2).

{B:1, D:2, C:inf, E:inf, F:inf, G:inf}. ((A: 0)).

Pop B.

{D:2, C:inf, E:inf, F:inf, G:inf}. ((A: 0, B: 1)).

changePriority(C, 4).

{D:2, C:4, E:inf, F:inf, G:inf}. ((A: 0, B: 1)).

Pop D.

{C:4, E:inf, F:inf, G:inf}. ((A: 0, B: 1, D: 2)).

changePriority(E, 5).

{C:4, E:5, F:inf, G:inf}. ((A: 0, B: 1, D: 2)).

Pop C.

{E:5, F:inf, G:inf}. ((A: 0, B: 1, D: 2, C: 4)).

changePriority(F, 6). changePriority(G, 8).

{E:5, F:6, G:8}. ((A: 0, B: 1, D: 2, C: 4)).

Pop E.

{F:6, G:8}. ((A: 0, B: 1, D: 2, C: 4, E: 5)).

potentialDistToG = 8, which is the same. No change priority.

Pop F.

{G:8}. ((A: 0, B: 1, D: 2, C: 4, E: 5, F: 6)).

potentialDistToG = 7. changePriority(G, 7).

{G:7}. ((A: 0, B: 1, D: 2, C: 4, E: 5, F: 6)).

Pop G.

{}. ((A: 0, B: 1, D: 2, C: 4, E: 5, F: 6, G: 7)).


(b) Given the weights and heuristic values for the graph below, what path would
A* search return, starting from A and with G as a goal?

Edge weights Heuristics
g(A, B) = 1 h(A, G) = 8
g(B, C) = 3 h(B, G) = 6
g(C, F) = 4 h(C, G) = 5
g(C, G) = 4 h(F, G) = 1
g(F, G) = 1 h(D, G) = 6
g(A, D) = 2 h(E, G) = 3
g(D, E) = 3
g(E, G) = 3

node distTo[] edgeTo[] h(v,u)
A	 0		  -		   8
B	 1		  A		   6
C	 4		  B		   5
D	 2		  A		   6
E	 5		  D		   3
F	 inf	  -		   1
G	 8		  E		   -

Fringe: [(G:8), (C:9), (F:inf)]
A* would return A − D − E − G. The cost here is 2 + 3 + 3 = 8.
