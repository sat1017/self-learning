Question 1: Graph Representations

(a) Write the graph above as an adjacency matrix, then as an adjacency list. What
would be different if the graph were undirected instead?

Directed:

Adj Matrix:
  A B C D E F G <- end node
A 0 1 0 1 0 0 0
B 0 0 1 0 0 0 0
C 0 0 0 0 0 1 0
D 0 1 0 0 1 1 0
E 0 0 0 0 0 1 0
F 0 0 0 0 0 0 0
G 0 0 0 0 0 1 0
ˆ start node

Adj List:
A: {B, D}
B: {C}
C: {F}
D: {B, E, F}
E: {F}
F: {}
G: {F}

Un-Directed:

Adj Matrix:
  A B C D E F G <- end node
A 0 1 0 1 0 0 0
B 1 0 1 1 0 0 0
C 0 1 0 0 0 1 0
D 1 1 0 0 1 1 0
E 0 0 0 1 0 1 0
F 0 0 1 1 1 0 1
G 0 0 0 0 0 1 0
ˆ start node

Adj List:
A: {B, D}
B: {A, C, D}
C: {B, F}
D: {A, B, E, F}
E: {D, F}
F: {C, D, E, G}
G: {F}

(b) Give the DFS preorder, DFS postorder, and BFS order of the graph traversals
starting from vertex A. Break ties alphabetically.

DFS Preorder: A,B,C,F,D,E   G
DFS Postorder: F,C,B,E,D,A   G
BFS: A,B,D,C,E,F   G
