package disc04;

public class IntListTest {

    @Test
    public void testList() {
        IntList one = new IntList(1, null);
        IntList twoOne = new IntList(2, one);
        IntList threeTwoOne = new IntList(3, twoOne);
        // 3 --> 2 --> 1   {3, 2, 1}
        IntList x = IntList.list(3, 2, 1);
        assertEquals(threeTwoOne, x);
    }
    @Test
    public void testdSquareList() {
        IntList L = IntList.list(1, 2, 3);
        IntList.dSquareList(L);
        assertEquals(Int.list(1,4,9), L);
        }
}
