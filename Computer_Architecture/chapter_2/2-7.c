#include <stdio.h>

unsigned invert(unsigned x, int p, int n);

int main(void)
{
  unsigned x; /* bits to update */
  int p; /* begining position in x to replace with n-bits from y */
  int n; /* number of bits in y to replace at position p in x */

  x = 7; /* for example in 8-bit 0000 1000 */
  p = 3;
  n = 2;
  printf("%ud\n", invert(x, p, n));
  
  return 0;
}

/* inverts x with n-bits starting at position p */
unsigned invert(unsigned x, int p, int n)
{
  return x ^ (~(~0 << n) << (p+1-n));
}
