#include <stdio.h>
#include <math.h>

#define MAXLINE 1000 /* max input line size */

int htoi(char s[]);
int getLine(char s[], int lim);
  
/* convert a string of hexadecimal to int */
int main(void)
{
  char line[MAXLINE]; /* current input line */
  int len;

  while ((len = getLine(line, MAXLINE)) > 0) {
	printf("%d\n", htoi(line));
	/* printf("%d\n", len); */
  }

}

/* converts a hexadecimal string to integers  */
int htoi(char s[])
{
  /* 1) get len of string
	 2) pow = 0; sum = 0
	 3) convert s[len] to int
	 4) multiply value of step 3 by 16^pow
	 5) ++pow and --len
	 6) repeat steps 3-5 and sum step 4
  */

  int i;
  int sum;
  int exp;
  int hex;

  /* get len of string */
  i = 0;
  while (s[i] != '\n')
	++i;

  --i;
  sum = 0;
  for (exp=0; i>=0; --i) {
	if (s[i] >= '0' && s[i] <= '9') {
	  hex = s[i] - '0';
	  sum = sum + (hex * pow(16, exp));
	}
	else if (s[i] >= 'A' && s[i] <= 'F') {
	  hex = s[i] - 'A' + 10;
	  sum = sum + (hex * pow(16, exp));
	}
	else if (s[i] >= 'a' && s[i] <= 'f') {
	  hex = s[i] - 'a' + 10;
	  sum = sum + (hex * pow(16, exp));
	}
	++exp;
  }

  return sum;

}

int getLine(char s[], int lim)
{
  int c, i;
  enum loop { NO, YES };
  enum loop state = YES;
  i = 0;
  
  while (state == YES) {
	if (i >= lim - 1)
	  state = NO;
	else if ((c=getchar()) == EOF)
	  state = NO;
	else if (c == '\n') {
	  s[i] = c;
	  ++i;
	  s[i] = '\0';
	  state = NO;
	}
	else {
	  s[i] = c;
	  ++i;
	}
  }
  
  return i;
}
