#include <stdio.h>

unsigned rightrot(unsigned x, int n);
int bitlen(void);

int main(void)
{
  unsigned x; /* bits to update */
  int n; /* number of bits in y to replace at position p in x */

  x = 7; /* for example in 8-bit 0000 1000 */
  n = 2;
  printf("%ud\n", rightrot(x, n));
  
return 0;
}

/* rotate x to the right by n-bits */
unsigned rightrot(unsigned x, int n)
{

  int i;

  for (i = 0; i < n; i++) {
	int temp;
	temp = x % 2; /* odd numbers will always have a 1 in the LSB */
	x = x >> 1; /* shift x 1-bit to the right */
	temp = temp << (bitlen() - 1); /* move  1st bit to the MSB */
	x = x | temp; /* OR both to add them  */
  }
  return x;                     
}


/* return the length of the bit */
int bitlen(void)
{

  int i;
  unsigned bit = (unsigned) ~0;
  for (i = 1; (bit = bit >> 1) > 0; i++)
	;
  return i;
}
