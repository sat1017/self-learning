#include <stdio.h>

unsigned setbits(unsigned x, int p, int n, unsigned y);

int main(void)
{
  unsigned x; /* bits to update */
  int p; /* begining position in x to replace with n-bits from y */
  int n; /* number of bits in y to replace at position p in x */
  unsigned y; /* bits to get n-bits from */ 

  x = 255; /* for example in 8-bit 0000 1000 */
  y = 7;
  printf("%ud\n", setbits(x, 5, 3, y));
  
  return 0;
}

unsigned setbits(unsigned x, int p, int n, unsigned y)
{
  return x & ~(~(~0 << n) << (p+1-n)) | (y & ~(~0 << n)) << (p+1-n);
}
