#include <stdio.h>

int any(char s1[], char s2[]);

int main(void)
{
  char s1[] = "abccfd";
  char s2[] = "cde";
  
  printf("%d\n", any(s1, s2));
  
  return 0;
}


/* return the first location in string s1 where any char from string s2 occurs or -1 if none */
int any(char s1[], char s2[])
{

  int i; /* s1 index */
  int j; /* s2 index */

  for (i = 0; s1[i] != '\0'; i++) {
	for (j = 0; s2[j] != '\0'; j++)
	  if (s1[i] == s2[j])
		return i;
  }
  
  return -1;
}


