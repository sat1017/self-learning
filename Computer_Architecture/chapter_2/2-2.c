#include <stdio.h>
#define MAXLINE 1000 /*  maximum input line size */
#define LONGLINE 80

int getLine(char s[], int lim);

/*  print longest input line */
int main(void)
{
  int len; /*  current line length */
  char line[MAXLINE]; /*  current input line */

  /* when you call just 'line' you are passing the
	 pointer to the function */
  while ((len = getLine(line, MAXLINE)) > 0)
	  printf("%s", line);
  return 0;
}


/*  getline: read a line into s, return length
	s: empty array to access values by index
	lim: int
 */
int getLine(char s[], int lim)
{
  int c, i;
  enum loop { NO, YES };
  enum loop state = YES;
  i = 0;
  
  while (state == YES) {
	if (i >= lim - 1)
	  state = NO;
	else if ((c=getchar()) == EOF)
	  state = NO;
	else if (c == '\n') {
	  s[i] = c;
	  ++i;
	  s[i] = '\0';
	  state = NO;
	}
	else {
	  s[i] = c;
	  ++i;
	}
  }
  
  return i;
}
