#include <stdio.h>

static char daytab[2][13] = {
							 {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
							 {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
};
static int curyear = 2020;
static int curmonth = 9;
static int curday = 15;

int day_of_year(int year, int month, int day);
void month_day(int year, int yearday, int *pmonth, int *pday);

int main(void)
{
  int val;
  int m;
  int d;
  val = day_of_year(2021, 9, 15);
  month_day(2021, 259, &m, &d);
  printf("%d\n", val);
  printf("%d %d\n", m, d);
 
  return 0;
}


/* day_of_year: set day of year from month and day */
int day_of_year(int year, int month, int day)
{
  int i, leap;
  if (year > curyear || month > curmonth || day > curday) {
	printf("error: invalid date supplied");
	return -1;
  }
  
  leap = year%4 == 0 && year%100 != 0 || year%400 == 0;
  for (i = 1; i < month; i++)
	day += daytab[leap][i];
  return day;
}


/* month_day: set month, and from day of year */
void month_day(int year, int yearday, int *pmonth, int *pday)
{
  int i, leap;

  leap = year%4 == 0 && year%100 != 0 || year%400 == 0;
  for (i = 1; yearday > daytab[leap][i]; i++)
	yearday -= daytab[leap][i];
  if (year > curyear || yearday > 365) {
	printf("error: invalid date supplied");
	*pmonth = -1;
	*pday = -1;
  } else {
	*pmonth = i;
	*pday = yearday;
  }
}
