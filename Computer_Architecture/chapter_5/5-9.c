#include <stdio.h>

static char daytab[2][13] = {
							 {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
							 {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
};
static int curyear = 2020;
static int curmonth = 9;
static int curday = 15;

int day_of_year(int year, int month, int day);
void month_day(int year, int yearday, int *pmonth, int *pday);

int main(void)
{
  int val;
  int m;
  int d;
  val = day_of_year(2020, 9, 16);
  month_day(2020, 260, &m, &d);
  printf("%d\n", val);
  printf("%d %d\n", m, d);
 
  return 0;
}


/* day_of_year: set day of year from month and day */
int day_of_year(int year, int month, int day)
{
  int leap;
  char *ptr;
  
  leap = year%4 == 0 && year%100 != 0 || year%400 == 0;
  if (year > curyear || month > curmonth || day > daytab[leap][month]) {
	printf("error: invalid date supplied\n");
	return -1;
  }
  ptr = daytab[leap];			/* point to the correct row */
  while(month--) {
	day += *ptr++;
  }
  return day;
}

/* month_day: set month, and from day of year */
void month_day(int year, int yearday, int *pmonth, int *pday)
{
  int i, leap;
  char *ptr, *p;
  
  leap = year%4 == 0 && year%100 != 0 || year%400 == 0;
  p = ptr = daytab[leap];
  while(yearday > *++ptr) {
	yearday -= *ptr;
  }
  /* for (i = 1; yearday > daytab[leap][i]; i++) */
  /* 	yearday -= daytab[leap][i]; */
  if (year > curyear || yearday > 365) {
	printf("error: invalid date supplied");
	*pmonth = -1;
	*pday = -1;
  } else {
	*pmonth = ptr - p;			/* ask why line in solution works with * */
	*pday = yearday;
  }
}
