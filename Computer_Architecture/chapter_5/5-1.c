#include <stdio.h>
#include <ctype.h>

#define BUFSIZE 100

char buf[BUFSIZE];				/* buffer size */
int bufp = 0;					/* next free position in buf */

int getch(void);
void ungetch(int);


/* getch: get a (possibly pushed back) character */
int getch(void)
{
  return (bufp > 0) ? buf[--bufp] : getchar();
}


/* ungetch: push character back on input */
void ungetch(int c)
{
  if (bufp >= BUFSIZE)
	printf("ungetch: too many characters\n");
  else
	buf[bufp++] = c;
}


/* getint: get next integer from input into *pn */
int getint(int *pn)
{
  int c, d, sign;

  while (isspace(c=getch()))
	;
  if (!isdigit(c) && c != EOF && c != '-' && c != '+') {
	ungetch(c);
	return 0;
  }
  sign = (c == '-') ? -1 : 1;
  if (c == '+' || c == '-') {
	d =  c;
	if (!isdigit(c=getch())) {
	  if (c != EOF)
		ungetch(c);
	  ungetch(d);
	  return d;
	}
  } 
  
  for (*pn = 0; isdigit(c); c = getch()) {
	*pn = 10 * *pn + (c - '0');
  }
  *pn *= sign; 					/* pointer gets changed once there is a number so w/ - 1 the initialized pointer never gets updated */
  if (c != EOF) {
	ungetch(c);
  }
  return c;
}

int main(void)
{
  int n, array[BUFSIZE];

  for (n = 0; n < BUFSIZE && getint(&array[n]) != EOF; n++) { 
	printf("array[%d] = %d\n", n, array[n]);
  }
  return 0;
}
