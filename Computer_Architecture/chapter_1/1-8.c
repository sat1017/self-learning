#include <stdio.h>

main(void)
{
  int c, t, s, nl;
  t = 0;
  s = 0;
  nl = 0;
  
  printf("Input some characters, then press Ctrl+D.\n");
  while ((c = getchar()) != EOF) {
	if (c == '\n')
	  ++nl;
	if (c == '\t')
	  ++t;
	if (c == ' ')
	  ++s;
  }
  
  printf("new lines: %d\n", nl);
  printf("tabs: %d\n", t);
  printf("spaces: %d\n", s);

  return 0;
}
