#include <stdio.h>

#define LEN 128

int main(void) {

  int c, i;
  int hist[LEN];

  for (i = 0; i < LEN; ++i)
	hist[i] = 0;
  
  while ((c = getchar()) != EOF)
	++hist[c];

  for (i = 0; i < LEN; i++)
	printf("%d ", hist[i]);
	  
  return 0;
}
