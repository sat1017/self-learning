#include <stdio.h>

main()
{
  float fahr, celsius, lower, step;
  int upper;

  lower = 0;
  upper = 300;
  step = 20;

  celsius = lower;
  printf("Celsius Fahr\n");
  while (celsius <= upper) {
	fahr = (9.0/5.0) * celsius + 32.0;
	printf("%5.1f %6.1f\n", celsius, fahr);
	celsius = celsius + step;
  }
}
