#include <stdio.h>
#include <stdlib.h>				/* for atof() */
#include <ctype.h>
#include <string.h>
#include <math.h>				/* for fmod() */

#define MAXOP 100				/* max size of operand or operator */
#define NUMBER '0'				/* signal that a number was found */
#define MAXVAL 100				/* maximum depth of val stack */
#define BUFSIZE 100


int main(void)
{
  int c = '*';
  char s[MAXOP] = "sin";
  int i;


  printf("%c islower? %d\n", c, islower(c));
  
  return 0;
}
